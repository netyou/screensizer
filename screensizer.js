(function( $ ){

  $.fn.screensizer = function() {

    var display = [
            '<div></div>'
        ].join(''),
        $this = this.first(),
        initialWindowWidth = $this.outerWidth(true);
    
    var $output = $( display, {
            id: 'resize-output',
            css: {
                width:          '90px',
                position:       'fixed',
                fontSize:       '1.5em',
                top:            '0',
                left:           '0',
                cursor:         'pointer',
                borderRadius:   '0 0 5px 0',
                background:     '#E3E3E3',
                border:         '1px solid #BBB',
                boxShadow:      'inset 0 0 1px 1px #F6F6F6',
                color:          '#333',
                font:           'bold 12px/1 sans-serif',
                padding:        '8px',
                textAlign:      'left',
                textShadow:     '0 1px 0 white',
                lineHeight:     '1.5em'
            },
            html: initialWindowWidth + 'px<br>' + initialWindowWidth / 16 + 'em'
        }).appendTo(document.body);
    
    var calcWidth = function(){
        
        var width = $this.outerWidth(true);
        
        $output.html( width + 'px<br>' + width / 16 + 'em' );
    
    }
    
    $this.resize( calcWidth );
    
    // in case we can't resize add a manual resize
    $output.click( calcWidth );

    return this;
  };

})( jQuery );